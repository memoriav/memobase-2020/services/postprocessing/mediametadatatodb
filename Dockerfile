FROM python:3.8

WORKDIR /mediametadatatodb_app
ADD mediametadatatodb_app /mediametadatatodb_app/
ADD requirements.txt /mediametadatatodb_app/
WORKDIR /mediametadatatodb_app
RUN pip install -r requirements.txt
WORKDIR /
ADD setup.py /
RUN pip install -e .
ENTRYPOINT ["python"]
CMD ["/mediametadatatodb_app/main.py"]
