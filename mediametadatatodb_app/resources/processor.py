import logging

from mediametadatatodb_app.resources.indexer import Indexer
from mediametadatatodb_app.resources.reporter import Reporter


class RecordProcessor:
    def __init__(self):
        self.counter = 0
        self.indexer = Indexer()
        self.processed_records = dict()
        self.reporter = Reporter()

    @staticmethod
    def _parsing_errors(record) -> bool:
        return (
            RecordProcessor._parsing_failed_digital_object(record)
            or RecordProcessor._parsing_failed_thumbnail(record)
            or RecordProcessor._parsing_failed_audio_snippet(record)
        )

    @staticmethod
    def _parsing_failed_digital_object(record) -> bool:
        return "digital_object" in record and not record["digital_object"]["ok"]

    @staticmethod
    def _parsing_failed_thumbnail(record) -> bool:
        return "thumbnail" in record and not record["thumbnail"]["ok"]

    @staticmethod
    def _parsing_failed_audio_snippet(record) -> bool:
        return "audio_snippet" in record and not record["audio_snippet"]["ok"]

    def new_record(self, rec_id, headers):
        self.processed_records[rec_id] = {"headers": headers}

    def digital_object_ok(self, rec_id, data):
        logging.debug(f"Parsing of digital object resource for {rec_id} successful")
        self.counter += 1
        self.processed_records[rec_id]["digital_object"] = {
            "data": data,
            "ok": True,
            "ignored": False,
            "msg": "successful",
        }

    def digital_object_fail(self, rec_id, err):
        logging.warning(f"Parsing of digital object resource for {rec_id} failed")
        self.processed_records[rec_id]["digital_object"] = {
            "data": None,
            "ok": False,
            "ignored": False,
            "msg": "parsing failed" + f": {err}" if err else "",
        }

    def digital_object_ignore(self, rec_id, message):
        logging.info(f"Digital object resource for {rec_id} ignored")
        self.processed_records[rec_id]["digital_object"] = {
            "data": None,
            "ok": True,
            "ignored": True,
            "msg": message,
        }

    def thumbnail_ok(self, rec_id, data):
        logging.debug(f"Parsing of thumbnail resource for {rec_id} successful")
        self.counter += 1
        self.processed_records[rec_id]["thumbnail"] = {
            "data": data,
            "ok": True,
            "ignored": False,
            "msg": "successful",
        }

    def thumbnail_fail(self, rec_id, err):
        logging.warning(f"Parsing of thumbnail resource for {rec_id} failed")
        self.processed_records[rec_id]["thumbnail"] = {
            "data": None,
            "ok": False,
            "ignored": False,
            "msg": "parsing failed" + f": {err}" if err else "",
        }

    def audio_snippet_ok(self, rec_id, data):
        logging.debug(f"Parsing of audio snippet resource for {rec_id} successful")
        self.counter += 1
        self.processed_records[rec_id]["audio_snippet"] = {
            "data": data,
            "ok": True,
            "ignored": False,
            "msg": "successful",
        }

    def audio_snippet_fail(self, rec_id):
        logging.warning(f"Parsing of audio snippet resource for {rec_id} failed")
        self.processed_records[rec_id]["audio_snippet"] = {
            "data": None,
            "ok": False,
            "ignored": False,
            "msg": "parsing failed",
        }

    def abort(self, ex):
        logging.error("Indexing failed. Aborting...")
        for key in self.processed_records.keys():
            self.reporter.send_message(
                key,
                "FATAL",
                f"Indexing failed: {ex}",
                self.processed_records[key]["headers"],
            )

    def index(self):
        for key in self.processed_records.keys():
            record = self.processed_records[key]
            dig_obj_msg = (
                record["digital_object"]["msg"]
                if "digital_object" in record
                else "not available"
            )
            thumbnail_msg = (
                record["thumbnail"]["msg"] if "thumbnail" in record else "not available"
            )
            audio_snip_msg = (
                record["audio_snippet"]["msg"]
                if "audio_snippet" in record
                else "not available"
            )
            ok = True
            ignored = True
            err_msg = ""
            if (
                "digital_object" in record
                and not record["digital_object"]["ignored"]
                and not RecordProcessor._parsing_failed_digital_object(record)
            ):
                logging.debug(f"Indexing digital object for {key} in DB")
                ignored = False
                ok, err_msg = self.indexer.insert_in_db(
                    record["digital_object"]["data"]
                )
            if (
                ok
                and "thumbnail" in record
                and not record["thumbnail"]["ignored"]
                and not RecordProcessor._parsing_failed_thumbnail(record)
            ):
                logging.debug(f"Indexing thumbnail for {key} in DB")
                ignored = False
                ok, err_msg = self.indexer.insert_in_db(record["thumbnail"]["data"])
            if (
                ok
                and "audio_snippet" in record
                and not record["audio_snippet"]["ignored"]
                and not RecordProcessor._parsing_failed_audio_snippet(record)
            ):
                logging.debug(f"Indexing audio snippet for {key} in DB")
                ignored = False
                ok, err_msg = self.indexer.insert_in_db(record["audio_snippet"]["data"])
            if ok and not RecordProcessor._parsing_errors(record):
                self.indexer.commit()
                self.reporter.send_message(
                    key,
                    "IGNORE" if ignored else "SUCCESS",
                    (
                        "DIGITAL OBJECT: {} -- THUMBNAIL: {}" + " -- AUDIO SNIPPET: {}"
                    ).format(dig_obj_msg, thumbnail_msg, audio_snip_msg),
                    record["headers"],
                )
            elif ok:
                self.indexer.commit()
                self.reporter.send_message(
                    key,
                    "FATAL",
                    (
                        "DIGITAL OBJECT: {} -- THUMBNAIL: {} " + "-- AUDIO SNIPPET: {}"
                    ).format(dig_obj_msg, thumbnail_msg, audio_snip_msg),
                    record["headers"],
                )
            else:
                self.indexer.rollback()
                self.reporter.send_message(
                    key, "FATAL", f"Indexing failed: {err_msg}", record["headers"]
                )
        self.processed_records.clear()
