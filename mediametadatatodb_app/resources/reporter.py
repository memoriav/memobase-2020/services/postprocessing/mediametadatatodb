#  mediametadatatodb
#  Copyright (C) 2020  Memoriav
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import json
import logging
import os
import time

from kafka.producer import KafkaProducer


class Reporter:
    def __init__(self):
        self._producer = self._connect_to_kafka()
        self._step_version = os.environ["APP_VERSION"]

    def _connect_to_kafka(self, retries=0):
        try:
            return KafkaProducer(
                bootstrap_servers=os.environ["KAFKA_BOOTSTRAP_SERVERS"],
                api_version=(3, 8, 0),
                security_protocol=os.environ["KAFKA_SECURITY_PROTOCOL"],
                ssl_check_hostname=True,
                ssl_cafile=os.environ["KAFKA_SSL_CA_LOCATION"],
                ssl_certfile=os.environ["KAFKA_SSL_CERTIFICATE_LOCATION"],
                ssl_keyfile=os.environ["KAFKA_SSL_KEY_LOCATION"],
            )
        except Exception as ex:
            logging.error("Exception while connecting Kafka: {}".format(ex))
            if retries < int(os.environ["KAFKA_CONNECTION_RETRIES"]):
                time.sleep(30 * (retries + 1))
                self._connect_to_kafka(retries + 1)

    def send_message(self, identifier, status, message, headers):
        try:
            key_bytes = bytes(
                identifier if identifier else "<unknown>", encoding="utf-8"
            )
            now = datetime.datetime.now()
            # //@formatter:off
            timestamp = now.strftime("%Y-%m-%dT%H:%M:%S.") + str(
                round(int(now.strftime("%f")) / 1000)
            )
            # //@formatter:on
            report = {
                "id": identifier,
                "status": status,
                "message": message,
                "step": "10.01-media-metadata-indexer",
                "stepVersion": self._step_version,
                "timestamp": timestamp,
            }
            json_report = json.dumps(report)
            value_bytes = bytes(json_report, encoding="utf-8")
            logging.debug(
                f'Sending report to {os.environ["TOPIC_PROCESS"]}: {json_report}'
            )
            self._producer.send(
                os.environ["TOPIC_PROCESS"],
                key=key_bytes,
                value=value_bytes,
                headers=headers,
            )
            self._producer.flush()
        except Exception as ex:
            logging.error("Couldn't send processing report!: {}".format(ex))
