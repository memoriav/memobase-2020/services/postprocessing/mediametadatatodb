import logging
import os
import time

import mysql.connector as mariadb


# noinspection SqlResolve,SqlNoDataSourceInspection
class Indexer:
    def __init__(self):
        """
        Start MariaDB client
        """
        self.mariadb_connection, self.mariadb_cursor = self._connect_to_mariadb()

    def _connect_to_mariadb(self, retries=0):
        """
        Connect to MariaDB. Abort after configured retries.
        """
        try:
            logging.debug(
                f"Connecting to DB {os.environ['MARIADB_DATABASE']} on "
                f"{os.environ['MARIADB_HOST']}:{os.environ['MARIADB_PORT']}"
            )
            mariadb_connection = mariadb.connect(
                user=os.environ["MARIADB_USER"],
                password=os.environ["MARIADB_PASSWORD"].rstrip(),
                host=os.environ["MARIADB_HOST"],
                port=int(os.environ["MARIADB_PORT"]),
                database=os.environ["MARIADB_DATABASE"],
                ssl_ca=os.environ["MARIADB_HOST_CERTS"],
                ssl_verify_cert=True
            )
            mariadb_connection.autocommit = False
            mariadb_cursor = mariadb_connection.cursor()
            return mariadb_connection, mariadb_cursor
        except Exception as ex:
            status = "Exception: " + str(ex)
            logging.error(status)
            if retries < int(os.environ["MARIADB_CONNECTION_RETRIES"]):
                time.sleep(30 * (retries + 1))
                self._connect_to_mariadb(retries + 1)
            exit(1)

    @staticmethod
    def _create_sql_stmt(table_name, record, fields) -> (str, tuple):
        """
        Create SQL statement
        """
        db_values = [record[f] for f in fields if f in record and record[f]]
        db_fields = ",".join([f for f in fields if f in record and record[f]])
        db_value_placeholders = ", ".join(
            ["%s" for f in fields if f in record and record[f]]
        )
        # noinspection SqlNoDataSourceInspection
        sql = "REPLACE INTO {} ({}) VALUES ({})".format(
            table_name, db_fields, db_value_placeholders
        )
        return sql, tuple(db_values)

    def insert_in_db(self, record) -> (bool, str):
        """
        Insert record in DB
        """
        stmt, values = Indexer._create_sql_stmt(
            "entities", record, ["sig", "uri", "access", "proto"]
        )
        try:
            self.mariadb_cursor.execute(stmt, values)
            stmt, values = Indexer._create_sql_stmt(
                "metadata",
                record,
                ["sig", "mimetype", "height", "width", "duration", "type"],
            )
            self.mariadb_cursor.execute(stmt, values)
            return True, ""
        except mariadb.Error as ex:
            logging.error(
                f'Problems in sql statement (statement: "{stmt}", '
                f"parameters: {values}): {ex}"
            )
            return False, str(ex)

    def commit(self):
        """
        Commit changes to DB
        """
        logging.debug("Commiting changes to DB")
        self.mariadb_connection.commit()

    def rollback(self):
        """
        Rollback changes
        """
        logging.info("Rollback changes")
        self.mariadb_cursor.reset()
        self.mariadb_connection.rollback()
