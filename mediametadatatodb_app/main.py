#  mediametadatatodb
#  Copyright (C) 2020  Memoriav
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os

from mediametadatatodb_app.resources.MediametadataToDB import MediametadataToDB

if __name__ == "__main__":
    numeric_level = getattr(logging, os.getenv("LOG_LEVEL").upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f'Invalid log level: {os.getenv("LOG_LEVEL")}')
    logging.basicConfig(
        format="%(levelname)-8s [%(filename)s:%(lineno)d] %(message)s",
        level=numeric_level,
    )
    logging.info("Starting up")
    runner = MediametadataToDB()
    runner.run()
    logging.info("Shutting down")
