# mediametadatatodb

Responsible for ingesting media metadata to the media server index.

This service takes a number of core metadata in order to allow the media server to correctly deliver media files. Among this metadata are information on:
* Mime type of media file
* Technical metadata: Aspect ratio (videos, images), duration (audios, videos)
* Player type which is used to replay the media content (either local player or one of several external players)
* Access settings (public, closed, and some others)
* Mode of access (redirect, via proxy, delivery of local file)

## Configuration

mediametadatatodb expects a couple of set environment variables to work properly:

* `APP_VERSION`: Application version used in reports
* `LOG_LEVEL`: Logging level of application (`DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`)
* `URI_BASE`: Path to root directory of local media files as the media server sees it (i.e. how the media directory is mounted inside the media server container). Has to be an absolute path with the `file://` prefix.
* `GROUP_ID`: Kafka consumer group id. See [Kafka documentation](https://kafka.apache.org/documentation/#consumerconfigs_group.id) for details
* `TOPIC_IN`: Name of Kafka input topic (where the message are coming from)
* `TOPIC_PROCESS`: Name of Kafka reports topic (where the status reports are written to)
* `KAFKA_CONNECTION_RETRIES`: Max number of retries to connect to Kafka cluster
* `KAFKA_BOOTSTRAP_SERVERS`: Comma-separated list of Kafka bootstrap server addresses
* `KAFKA_SECURITY_PROTOCOL`: Protocol used to communicate with brokers. Valid values are: `PLAINTEXT`, `SSL`, `SASL_PLAINTEXT`, `SASL_SSL`
* `KAFKA_SSL_CA_LOCATION`: Path to trusted certificates in PEM format with X.509 certificates
* `KAFKA_SSL_CERTIFICATE_LOCATION`: Path to client certificate in PEM format.
* `KAFKA_SSL_KEY_LOCATION`: Path to private key in PEM format with PKCS#8 keys. If the key is encrypted, key password must be specified using `KAFKA_SSL_KEY_PASSWORD`
* `MARIADB_HOST_CERTS`: Path to MariaDB CA certificates
* `MARIADB_CONNECTION_RETRIES`: Max number of retries to connect to MariaDB

The default settings for the Memobase environment can be found in [helm-charts/helm-values](./helm-charts/helm-values).
