#  mediametadatatodb
#  Copyright (C) 2020  Memoriav
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from mediametadatatodb_app.resources import indexer


# class Test(unittest.TestCase):
# noinspection SqlNoDataSourceInspection,SqlResolve
# def test__create_sql_stmt(self):
#     record = {
#         "sig": "test-001",
#         "mimetype": "image/jpeg",
#         "height": 20,
#         "width": 100,
#         "type": "image",
#     }
#     metadata_stmt, metadata_values = indexer.Indexer._create_sql_stmt(
#         "metadata",
#         record,
#         ["sig", "mimetype", "height", "width", "duration", "type"],
#     )
#     self.assertEqual(
#         (
#             "INSERT INTO metadata (sig,mimetype,height,width,type) VALUES"
#             " (%s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE mimetype=%s, height=%s,"
#             " width=%s, type=%s",
#             (
#                 "test-001",
#                 "image/jpeg",
#                 20,
#                 100,
#                 "image",
#                 "image/jpeg",
#                 20,
#                 100,
#                 "image",
#             ),
#         ),
#         (metadata_stmt, metadata_values),
#     )
