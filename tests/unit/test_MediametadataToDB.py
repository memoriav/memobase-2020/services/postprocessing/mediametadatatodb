#  mediametadatatodb
#  Copyright (C) 2020  Memoriav
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
from pathlib import Path
from unittest import TestCase

from mediametadatatodb_app.resources import MediametadataToDB


class Test(TestCase):
    @staticmethod
    def _load_file_and_get_res(file_name, get_res):
        path = Path(__file__).parent / f"../fixtures/{file_name}"
        with path.open("r") as f:
            json_msg = json.load(f)
        return get_res(json_msg["@graph"])

    @staticmethod
    def _get_digital_object(json_obj):
        for res in json_obj:
            if "type" in res and res["type"] == "digitalObject":
                return res

    @staticmethod
    def _get_record(json_obj):
        for res in json_obj:
            if "@type" in res and res["@type"] == "rico:Record":
                return res

    @staticmethod
    def _get_thumbnail(json_obj):
        for res in json_obj:
            if "type" in res and res["type"] == "thumbnail":
                return res

    def setUp(self) -> None:
        os.environ["URI_BASE"] = "file:///data/"

    def test__get_access_status_if_resource_is_private(self):
        access_status = MediametadataToDB._get_access_status(
            Test._load_file_and_get_res("BAB-PA_43-BAB_MC169A.json", lambda x: x),
            "some_rec",
        )
        self.assertEqual(access_status, "closed")

    def test__get_access_status_if_multiple_access_flags(self):
        access_status = MediametadataToDB._get_access_status(
            Test._load_file_and_get_res(
                "rtr-003-be51ef77-7963-46ff-a609-da4041779a5a_01.json", lambda x: x
            ),
            "some_rec",
        )
        self.assertEqual(access_status, "public")

    def test__get_access_status_if_resource_is_public(self):
        access_status = MediametadataToDB._get_access_status(
            Test._load_file_and_get_res("mfk-FLM-167202.json", lambda x: x), "some_rec"
        )
        self.assertEqual(access_status, "public")

    def test__get_values_from_inaccessible_digital_object(self):
        res = {
            "sig": "BAB-PA_43-BAB_MC169A-1",
            "access": "public",
            "proto": "redirect",
            "type": "audio",
            "uri": "rtmp://intstream.memobase.ch:1935/memobase/mp3:BAB_MC169A.mp3",
        }
        digital_object = Test._load_file_and_get_res(
            "BAB-PA_43-BAB_MC169A.json", Test._get_digital_object
        )
        dig_obj_val = MediametadataToDB._extract_dig_obj_vals(digital_object, "public")
        self.assertDictEqual(dig_obj_val, res)

    def test__get_values_from_closed_digital_object(self):
        res = {
            "sig": "mfk-FLM-167202-1",
            "access": "private",
            "mimetype": "video/mpeg",
            "height": "384",
            "width": "512",
            "duration": 524,
            "uri": "http://datenbanksammlungen.mfk.ch/eMP/"
            + "eMuseumPlus?service=MultimediaAsset&objectId=144755&memobaseExt=mp4",
            "type": "video",
            "proto": "proxydirect",
        }
        digital_object = Test._load_file_and_get_res(
            "mfk-FLM-167202.json", Test._get_digital_object
        )
        dig_obj_val = MediametadataToDB._extract_dig_obj_vals(digital_object, "private")
        self.assertDictEqual(dig_obj_val, res)

    def test__get_values_from_accessible_digital_object(self):
        res = {
            "sig": "mfk-FLM-167202-1",
            "access": "public",
            "mimetype": "video/mpeg",
            "height": "384",
            "width": "512",
            "duration": 524,
            "uri": "http://datenbanksammlungen.mfk.ch/eMP/"
            + "eMuseumPlus?service=MultimediaAsset&objectId=144755&memobaseExt=mp4",
            "type": "video",
            "proto": "redirect",
        }
        digital_object = Test._load_file_and_get_res(
            "mfk-FLM-167202.json", Test._get_digital_object
        )
        dig_obj_val = MediametadataToDB._extract_dig_obj_vals(digital_object, "public")
        self.assertDictEqual(dig_obj_val, res)

    def test__proto_flag_for_private_fonoteca_resource(self):
        digital_object = Test._load_file_and_get_res(
            "snp-007-218669_06.json", Test._get_digital_object
        )
        res = MediametadataToDB._extract_dig_obj_vals(digital_object, "private")
        self.assertEqual(res["proto"], "redirect")

    # def test__get_values_from_thumbnail(self):
    #     res = {
    #         "uri": "file:///data/Tanzarchiv-34695-35298-1-poster.jp2",
    #         "access": "public",
    #         "mimetype": "image/jp2",
    #         "type": "image",
    #         "proto": "file",
    #         "sig": "Tanzarchiv-34695-35298-1-poster",
    #     }
    #     thumbnail = Test._load_file_and_get_res(
    #         "Tanzarchiv-34695-35298.json", Test._get_thumbnail
    #     )
    #     thumbnail_val = MediametadataToDB._extract_thumbnail_values(thumbnail, "public")
    #     self.assertDictEqual(thumbnail_val, res)

    def test__has_poster(self):
        res = {
            "access": "public",
            "height": "544",
            "mimetype": "image/jpeg",
            "proto": "redirect",
            "sig": "rtr-003-002ed83f-8892-4d1d-a117-e30a42f8d627_01-1-poster",
            "type": "image",
            "uri": "https://ws.srf.ch/asset/image/audio/e931a47f-48d8-4ce5-8dcb-81d457b902a3/EPISODE_IMAGE/1578358849.png",
            "width": "960",
        }
        thumbnail = Test._load_file_and_get_res(
            "rtr-003-002ed83f-8892-4d1d-a117-e30a42f8d627_01.json", Test._get_thumbnail
        )
        thumbnail_val = MediametadataToDB._extract_thumbnail_values(thumbnail, "public")
        self.assertDictEqual(thumbnail_val, res)

    def test__has_no_audio_snippet(self):
        record_values_for_db = []
        for recordJsonData in Test._load_file_and_get_res(
            "SRF-UMusik-BE_MG_DUR_B62276.json", lambda x: x
        ):
            if "type" in recordJsonData and recordJsonData["type"] == "digitalObject":
                record_values_for_db.append(
                    MediametadataToDB._extract_fields(
                        recordJsonData,
                        MediametadataToDB._extract_dig_obj_vals,
                        "public",
                    )[0]
                )
        self.assertFalse(
            [
                MediametadataToDB._has_audio_snippet(record)
                for record in record_values_for_db
            ][0]
        )

    def test__has_mimetype(self):
        digital_object = Test._load_file_and_get_res(
            "maa-003-SF_2019-0006.json", Test._get_digital_object
        )
        digital_object = MediametadataToDB._extract_dig_obj_vals(digital_object, "public")
        print(digital_object)
        self.assertEqual(digital_object["uri"], "file:///data/maa-003-SF_2019-0006-1.jpg")

    # def test__has_audio_snippet(self):
    #    record_values_for_db = []
    #    for recordJsonData \
    #    in Test._load_file_and_get_res('Tanzarchiv-34695-35298.json', lambda x: x):
    #        if 'type' in recordJsonData and \
    #                recordJsonData['type'] == 'digitalObject':
    #            MediametadataToDB._try_fetch_from_json_object(recordJsonData, record_values_for_db,
    #                                                          MediametadataToDB.
    #                                                          _get_values_from_digital_object,
    #                                                          'public')
    #    self.assertTrue([MediametadataToDB._has_audio_snippet(record)
    #    for record in record_values_for_db][0])
